package tsp;
import graph.*;


public class TSP {
	
	public Solution BruteForceAlgo(Graph G)
	{
		Solution s = new Solution();
		return s;
	}
	
	public Solution PolyAlgo(Graph G)
	{
		Solution s = new Solution();
		return s;
	}
	
	public void printPath(int [] A)
	{
		for(int i=0;i<A.length;i++)
		{
			System.out.print(A[i] + " ");
		}
		System.out.println();
	}
	
	public void allPaths(int [] A)
	{
		printPath(A);
		permute(A,0,A.length-1);
	}
	
	public void permute(int []A,int l,int r)
	{
		if (l == r) 
             printPath(A);
        else
        { 
            for (int i = l; i <= r; i++) 
            { 
                swap(A,l,i); 
                permute(A, l+1, r); 
                swap(A,l,i); 
            } 
        } 
	}
	public void swap(int [] A, int i, int j) 
    { 
        int temp; 
        temp = A[i] ; 
        A[i] = A[j]; 
        A[j] = temp; 
    }	
}
