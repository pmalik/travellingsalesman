package graph;

import java.util.HashMap;

public class Node {

	int cityNum;
	double x_coordinate;
	double y_coordinate;
	HashMap<Integer, Double> neighbors;
	
	public Node(int cityNum,double x_coordinate,double y_coordinate) {
	
		this.cityNum= cityNum;
		this.x_coordinate = x_coordinate;
		this.y_coordinate = y_coordinate;
		neighbors = new HashMap<Integer, Double>();
	}
}
