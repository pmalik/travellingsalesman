package graph;

import java.util.ArrayList;
import java.util.HashMap;

public class Graph 
{
	ArrayList<Node> allVertex;
	public Graph() {
		allVertex = new ArrayList<Node>();
	}
	
	public void addVertex(int cityNum,double x_coordinate, double y_coordinate)
	{
		Node n = new Node(cityNum,x_coordinate,y_coordinate);
		allVertex.add(n);
	}
	
	public void MakeCompleteGraph()
	{
		//Iterate over each vertex in allVertex
		for (Node temp : allVertex) {
			addEdges(temp.cityNum, temp.x_coordinate,temp.y_coordinate, temp.neighbors);
		}
	}
	
	public void addEdges(int cityNum, double x1,double y1, HashMap<Integer, Double> neighbors)
	{
		for(Node temp : allVertex)
		{
			if(temp.cityNum != cityNum)
			{
				neighbors.put(temp.cityNum, (calculateDistance(x1, y1, temp.x_coordinate, temp.y_coordinate)));
			}
		}
	}
	
	public double calculateDistance(double x1, double y1, double x2, double y2)
	{
		double distance =0.0;
		
		return distance;
	}
	
	public void showGraph()
	{
		for(Node temp: allVertex)
		{
			System.out.println(temp.cityNum);
		}
	}
}



















