package driver;

import graph.Graph;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import tsp.*;
public class Driver {
	
	public static void main(String[] args) 
	{
		Graph testGraph1 = new Graph();
		Graph testGraph2 = new Graph();
		String FileName;
		
		FileName = "/home/coderinmordor/Desktop/Personal_Projects/TSP_Solution/src/bench1.txt";
		MakeGraph(FileName,testGraph1);
		
		FileName = "/home/coderinmordor/Desktop/Personal_Projects/TSP_Solution/src/bench2.txt";
		MakeGraph(FileName,testGraph2);
		
		//Run the Brute-Force algorithm
		TSP tsp = new TSP();
		
		Solution bruteForce = tsp.BruteForceAlgo(testGraph1);
		Solution polyAlgo = tsp.PolyAlgo(testGraph1);
		
	}
	
	public static void MakeGraph(String FileName, Graph G)
	{
		ReadData(FileName,G);
		G.MakeCompleteGraph();
	}
	
	public static void ReadData(String FileName, Graph G)
	{
		File file = new File(FileName);
		BufferedReader br;
		try
		{
			br = new BufferedReader(new FileReader(file));
			String st; 
			while ((st = br.readLine()) != null)
			{
				st.trim();
				String[] parts = st.split(" ");
				G.addVertex(Integer.parseInt(parts[0]),Double.parseDouble(parts[1]),Double.parseDouble(parts[1]));
			}				 
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} catch (IOException e) 
		{
			e.printStackTrace();
		}  
	}
	
} // End of class